$(document).ready(function () {
	var start_date = $('.hidden__start-date').text();
	$('.countdown__widget').countdown(start_date)
		.on('update.countdown', function (event) {
			$(this).html(event.strftime(
				`<div class="countdown__widget-part days">
	            <div class="countdown__widget-number">%D</div>
	            <span class="countdown__widget-text">дней</span>
	        </div>
	        <div class="countdown__widget-part hours">
	            <div class="countdown__widget-number">%H</div>
	            <span class="countdown__widget-text">часов</span>
	        </div>
	        <div class="countdown__widget-part minutes">
	            <div class="countdown__widget-number">%M</div>
	            <span class="countdown__widget-text">минут</span>
	        </div>`
			));
		});

	$('.up__table').scrollbar();

	$('.menu-mobile').click(function (e) {
		if($('.menu').find('.total-lub').length == 0) {
			$('.total-lub').appendTo('.menu__ul');
		}
		if($('.menu').find('.profile').length == 0) {
			$('.profile').appendTo('.menu__ul');
		}
		$('.menu').toggleClass('active');
	})

	function OpenPopup(popupId) {
		$('body').removeClass('no-scrolling');
		$('.popup').removeClass('js-popup-show');
		popupId = '#' + popupId;
		$(popupId).addClass('js-popup-show');
		$('body').addClass('no-scrolling');
	}

	$('.pop-op').click(function(e){
		e.preventDefault();
		let data = $(this).data('popup');
		OpenPopup(data);
	});

	// $('.sub-form__submit').click(function (e) {
	// 	e.preventDefault();
	// 	$('.popup.popup-thx').addClass('js-popup-show');

	// 	$('body').addClass('no-scrolling');
	// })

	$('.menu_link_feedback').click(function (e) {
		e.preventDefault();
		$('.popup.popup-feedback').addClass('js-popup-show');
		
		$('body').addClass('no-scrolling');
		// $.ajax({
		// 	url: "test",
		// 	context: document.body
		// }).done(function () {
		// });
	})


	$(".sub-form form").submit(function (e) {
		e.preventDefault();
		$.ajax({
			url: "test",
			context: document.body
		}).done(function () {
			$('.popup.popup-thx').addClass('js-popup-show');

			$('body').addClass('no-scrolling');
		});
	})

	// $('.popup-thx-feedback-btn').click(function (e) {
	// 	e.preventDefault();

	// 	$('body').addClass('no-scrolling');

	// 	$('.popup.popup-thx-feedback').addClass('js-popup-show');
	// })

	$('.up__upload').click(function (e) {
		e.preventDefault();
		$('.up__upload').addClass('js-popup-show')
		// .addClass('js-popup-show-upload');

		$('body').addClass('no-scrolling');
	})



	function popUp() {
		// Close PopUp
		$('.js-close-popup').on('click', function (e) {
			e.preventDefault();
			$('.popup').removeClass('js-popup-show');
			$('body').removeClass('no-scrolling');
		});
	}
	popUp();



	//seclect2

	if ($('.select2').length){
		$('.feedback-select').select2({
			placeholder: 'Выберите тему',
			minimumResultsForSearch: Infinity
		});
		$('.weeks-select').select2({
			minimumResultsForSearch: Infinity,
			dropdownParent: $('.weeks-wrap')
		});
	}

	$('.table__rej-trigger').click(function (e) {
		e.preventDefault();
		var t = $(this).offset().top;
		$('.table__rej-tooltip.tooltip').css('top',t+50);
		$('.table__rej-tooltip.tooltip').toggleClass('active');
		$('body').on('click', function (e) {
	      	var div = $('.table__rej-tooltip.tooltip, .table__rej-trigger');

	      	if (!div.is(e.target) && div.has(e.target).length === 0) {
	        	$('.table__rej-tooltip.tooltip').removeClass('active');
	      	}
	    });
	});

	$('.tooltip .btn').click(function (e) {
		$(this).parent().removeClass('active');
	})

  	function checkValidate() {
    	var form = $('form');

	    $.each(form, function () {
	      	$(this).validate({
				ignore: [],
				errorClass: 'error',
				validClass: 'success',
				rules: {
					fname: {
						required: true
					},
					name: {
						required: true
					},
					sname: {
						required: true
					},
					email: {
            			required: true,
            			email: true 
            		},
          			phone: {
            			required: true,
            			phone: true 
            		},
					text: {
						required: true
					},
					'reg-acept': {
						required: true
					},
					'reg-privacy': {
						required: true
					},
					theme: {
						required: true
					},
					'text-contact': {
						required: true
					},

					password: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					code: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					ticket: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					},
					message: {
						required: true,
						normalizer: function normalizer(value) {
							return $.trim(value);
						}
					}
				},

				errorElement : 'span',
		    	errorPlacement: function(error, element) {
		          	var placement = $(element).data('error');
		          	if (placement) {
		            	$(placement).append(error);
		          	} else {
		        		error.insertBefore(element);
		          	}
		        },

				messages: {
					phone: {
						required: "Укажите номер телефона"
					},
					email: {
						required: "Введите правильный email"
					},
					theme: {
						required: "Выберите тему"
					},
					ph: {
						required: "Неверный номер телефона или пароль"
					}
				}
			});


	      	jQuery.validator.addMethod('letters', function (value, element) {
		        return this.optional(element) || /^([a-zа-яё]+)$/i.test(value);
	      	});

	      	jQuery.validator.addMethod('email', function (value, element) {
		        return this.optional(element) || /\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6}/.test(value);
	      	});

	      	jQuery.validator.addMethod('phone', function (value, element) {
		        return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
	      	});

	    });

  	}
  	checkValidate();

	// Masked Phone
	$('input[type="tel"], .phone-mask').mask('+7(999)999-99-99');

	//$('input[class="card-mask"]').mask('9999-9999-9999-9999');

});